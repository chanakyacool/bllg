class Post < ActiveRecord::Base
  attr_accessible :location, :message, :title
  validates_presence_of :title, :message, :location
  has_many :comments
end
